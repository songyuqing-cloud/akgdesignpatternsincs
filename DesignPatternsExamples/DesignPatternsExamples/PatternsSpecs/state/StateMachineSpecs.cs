using NUnit.Framework;
using Patterns.state;

namespace PatternsSpecs.state {
    
    [TestFixture]
    public class StateMachineSpecs {

        [Test]
        public void heavyObjectBasedStateMachineSpec() {
            var target = new OneButtonInterfaceStateGraph();
            Assert.That(target.currentStateRef, Is.SameAs(target.sleepStateRef));
            
            target.receivedEvent(Event.ShortPress); // nothing happens...
            Assert.That(target.currentStateRef, Is.SameAs(target.sleepStateRef));
            
            target.receivedEvent(Event.LongPress); // switches the device on...
            Assert.That(target.currentStateRef, Is.SameAs(target.powerOnStateRef));
            
            target.receivedEvent(Event.ShortPress); // prepares a drink
            Assert.That(target.currentStateRef, Is.SameAs(target.prepareDrinkStateRef));
            
            target.receivedEvent(Event.TimerDue); // drink is finished...
            Assert.That(target.currentStateRef, Is.SameAs(target.powerOnStateRef));
            
            target.receivedEvent(Event.DoubleShortPress); // switches to programming-mode...
            Assert.That(target.currentStateRef, Is.SameAs(target.programmingModeStateRef));
            
            target.receivedEvent(Event.TripleShortPress); // switches to power-on mode...
            Assert.That(target.currentStateRef, Is.SameAs(target.powerOnStateRef));
            
            target.receivedEvent(Event.LongPress); // switches to sleep mode...
            Assert.That(target.currentStateRef, Is.SameAs(target.sleepStateRef));
        }
        
        [Test]
        public void functionRepresentsStateLightweightMachineSpec() {
            var target = new OneButtonInterfaceFunctionRepresentsState();
            target.receivedEvent(Event.ShortPress); // nothing happens...
            target.receivedEvent(Event.LongPress); // switches the device on...
            target.receivedEvent(Event.ShortPress); // prepares a drink
            target.receivedEvent(Event.TimerDue); // drink is finished...
            target.receivedEvent(Event.DoubleShortPress); // switches to programming-mode...
            target.receivedEvent(Event.TripleShortPress); // switches to power-on mode...
            target.receivedEvent(Event.LongPress); // switches to sleep mode...
        }
    }
    
}