using System;
using System.Collections.Generic;
using NUnit.Framework;
using Patterns.decorator;

namespace PatternsSpecs.decorator {
    [TestFixture]
    public class DecoratorSpecs {

        public void showDecorators() {
            var innerWidget = new Widget();
            var logOutput = new List<string>();
            var withLoggingAdded = new WidgetLoggingDecorator(innerWidget, msg => logOutput.Add(msg));
            var withNarrowingAdded = new WidgetArgumentNarrowingDecorator(withLoggingAdded, 10, 30);
            var withWriteLock = new WidgetWriteLockDecorator(withNarrowingAdded);
            
            withLoggingAdded.justAValue = 12;
            Assert.That(logOutput[0], Is.EqualTo("setter of justAValue called"));
            logOutput.Clear();
            
            Assert.That(new Action(() => withNarrowingAdded.justAValue = 42),
                Throws.InstanceOf<ArgumentException>());
            
            Assert.That(new Action(() => withWriteLock.justAValue = 42),
                Throws.InstanceOf<InvalidOperationException>());

        }
    }
}