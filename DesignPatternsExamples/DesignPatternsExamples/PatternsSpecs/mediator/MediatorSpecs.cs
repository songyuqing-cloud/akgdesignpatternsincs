using NUnit.Framework;
using Patterns.mediator;

namespace PatternsSpecs.mediator {
    [TestFixture]
    public class MediatorSpecs {

        [Test]
        public void showFontChooserMediator() {
            var mediator = new FontChooserMediator();
            mediator.buildDialog();

            mediator.fontSelector.selectedFont = Font.Verdana;
            
            Assert.That(mediator.fontSelector.selectedFont, Is.EqualTo(Font.Verdana));
            Assert.That(mediator.textExamplePresenter.exampleFont, Is.EqualTo(Font.Verdana));

            mediator.textExamplePresenter.exampleFont = Font.Helvetica;
            
            Assert.That(mediator.fontSelector.selectedFont, Is.EqualTo(Font.Helvetica));
            Assert.That(mediator.textExamplePresenter.exampleFont, Is.EqualTo(Font.Helvetica));
        }
    }
}