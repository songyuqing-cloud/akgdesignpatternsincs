using System.Threading.Tasks;
using NUnit.Framework;
using Patterns.proxy;
using static Patterns.proxy.StatisticalCalculations;

namespace PatternsSpecs.proxy {
    [TestFixture]
    public class ProxySpecs {

        [Test]
        public void showGofOriginalProxy() {
            var statisticalValue = new StatisticalValue("this is a heavy formula");
            
            // statisticalValue.calculate() // this would take very long...
            
            // ... so we use a proxy
            var proxyAroundExpensiveCalculation = new ExpensivelyCalculatedValueProxy<double>(statisticalValue);
            proxyAroundExpensiveCalculation.triggerBackgroundCalculation();
            
            // we can perform other stuff here...

            // and now we finally really need the value... and it's already pre-calculated (sometimes :-) )
            Assert.That(proxyAroundExpensiveCalculation.calculate(), Is.EqualTo(42.0));
        }
        
        [Test]
        public void showModernBackgroundCalculation() {
            var calculation = Task<double>.Factory.StartNew(() => calculateStatistics("this is a heavy formula"));
            
            // we can perform other stuff here...

            // and now we fetch the value from the background calculation
            Assert.That(calculation.Result, Is.EqualTo(42.0));
        }
        
        [Test]
        public void showSlowConnectionSimulator() {
            var srcWidget = new Widget();
            var slowConnectionSimulatedWidget = new WidgetSlowConnectionSimulator(srcWidget);
            var target = new ClassNeedingWidget(slowConnectionSimulatedWidget);
            
            // first we do not simulate a delay
            var millisWithoutDelay = target.jumpFromTheCliff();
            
            // and now we simulate a delay
            slowConnectionSimulatedWidget.millisDelay = 1000;
            var millisWithDelay = target.jumpFromTheCliff();
            
            // attention: don't do the following in production-code, because for heavy loads
            // on a machine the stopwatch-based assumptions may fail...
            Assert.That(millisWithoutDelay, Is.LessThan(10L));
            Assert.That(millisWithDelay, Is.GreaterThan(500L));
        }
        
    }
}