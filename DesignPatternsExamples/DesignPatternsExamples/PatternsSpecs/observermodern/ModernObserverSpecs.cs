using System.Collections.Generic;
using NUnit.Framework;
using Patterns.observermodern;

namespace PatternsSpecs.observermodern {
    
    [TestFixture]
    public class ModernObserverSpecs {
        
        [Test]
        public void showModernObserver() {
            var widget0 = new ObservableWidget("0");
            var widget1 = new ObservableWidget("1");

            var changeNotifications0 = new List<int>();
            var changeNotifications1 = new List<int>();

            widget0.justAValueChanged += (src, oldValue, newValue) => { changeNotifications0.Add(newValue); };
            widget1.justAValueChanged += (src, oldValue, newValue) => { changeNotifications1.Add(newValue); };
          
            widget0.justAValue = 42;
            widget1.justAValue = 43;
            widget0.justAValue = 44;
            widget1.justAValue = 45;
            
            Assert.That(changeNotifications0[0], Is.EqualTo(42));
            Assert.That(changeNotifications0[1], Is.EqualTo(44));
            
            Assert.That(changeNotifications1[0], Is.EqualTo(43));
            Assert.That(changeNotifications1[1], Is.EqualTo(45));
        }
        
        [Test]
        public void showModernObserverWithCustomEventTable() {
            var widget0 = new ObservableWidgetWithCustomEventTable("0");
            var widget1 = new ObservableWidgetWithCustomEventTable("1");

            var changeNotifications0 = new List<int>();
            var changeNotifications1 = new List<int>();

            widget0.justAValueChanged += (src, oldValue, newValue) => { changeNotifications0.Add(newValue); };
            widget1.justAValueChanged += (src, oldValue, newValue) => { changeNotifications1.Add(newValue); };
          
            widget0.justAValue = 42;
            widget1.justAValue = 43;
            widget0.justAValue = 44;
            widget1.justAValue = 45;
            
            Assert.That(changeNotifications0[0], Is.EqualTo(42));
            Assert.That(changeNotifications0[1], Is.EqualTo(44));
            
            Assert.That(changeNotifications1[0], Is.EqualTo(43));
            Assert.That(changeNotifications1[1], Is.EqualTo(45));
        }
        
    }
    
}