using System;
using System.Collections.Generic;
using NUnit.Framework;
using Patterns.encapsulatedcalls;

namespace PatternsSpecs.encapsulatedcalls {
    
    public static class Comparisons {
        public static int compareToNullProof<TValue>(this TValue lhs, TValue rhs) where TValue : IComparable<TValue> {
            if (lhs == null)
                return rhs == null ? 0 : -1;
            return rhs == null ? 1 : lhs.CompareTo(rhs);
        }
    }
    
    public class InspectableValue {
        public string value;
        public List<string> inspectedBy = new List<string>();
    }

    public static class Inspections {
        public static InspectableValue inspect(this InspectableValue value, string inspector, Action<InspectableValue> inspectIt) {
            return value
                .afterDoing(inspectIt)
                .modifiedAs(inspectedValue => inspectedValue.inspectedBy.Add(inspector));
        }
    }

    [TestFixture]
    public class EncapsulatedCallsSpecs {

        class Name : IComparable<Name> {
            public string firstName;
            public string middleInitials;
            public string lastName;

            private int compareOldStyle(Name other) {
                var comparisonResult = lastName.compareToNullProof(other.lastName);
                if (comparisonResult != 0)
                    return comparisonResult;
                comparisonResult = firstName.compareToNullProof(other.firstName);
                if (comparisonResult != 0)
                    return comparisonResult;
                return middleInitials.compareToNullProof(other.middleInitials);
            }
            
            private int compareNewStyle(Name other) {
                return lastName.compareToNullProof(other.lastName)
                    .orIfEqual(() => firstName.compareToNullProof(other.firstName))
                    .orIfEqual(() => middleInitials.compareToNullProof(other.middleInitials));
            }
            
            public int CompareTo(Name other) {
                return compareNewStyle(other);
            }

            public override string ToString() {
                return firstName + " " + middleInitials + " " + lastName;
            }
        }
        
        [Test]
        public void hierarchicalComparisonExample() {
            var name0 = new Name {firstName = "Otto", lastName = "McLast", middleInitials = "J."};
            var name1 = new Name {firstName = "Hugo", lastName = "McFirst", middleInitials = "J."};
            var name2 = new Name {firstName = "Franz", lastName = "McLast", middleInitials = "J."};
            var name3 = new Name {firstName = "Otto", lastName = "McLast", middleInitials = "K."};

            Assert.That(name0, Is.GreaterThan(name2));
            Assert.That(name0, Is.GreaterThan(name1));
            Assert.That(name0, Is.LessThan(name3));
        }

        
        [Test]
        public void afterDoingAndModifiedAsExample() {
            var inspectableValue = new InspectableValue {value = "just a text"};
            inspectableValue.inspect("Klaus", 
                toInspect => { 
                    // important inspection here
            });
            
            Assert.That(inspectableValue.inspectedBy[0], Is.EqualTo("Klaus"));
        }
        
        [Test]
        public void doWithCleanupExample() {
            bool didSomething = false;
            bool cleanedUp = true;

            var result = EncapsulatedCalls.doWithCleanup(() => {
                didSomething = true;
                return 42;
            }, 
                () => { cleanedUp = true; });

            Assert.That(result, Is.EqualTo(42));
            Assert.That(didSomething, Is.True);
            Assert.That(cleanedUp, Is.True);
            
            didSomething = false;
            cleanedUp = false;
            var exceptionCaught = false;
            
            try {
                EncapsulatedCalls.doWithCleanup<int>(() => {
                        didSomething = true;
                        throw new InvalidOperationException();
                    },
                    () => { cleanedUp = true; });
            }
            catch (Exception exc) {
                exceptionCaught = true;
            }
            
            Assert.That(exceptionCaught, Is.True);
            Assert.That(didSomething, Is.True);
            Assert.That(cleanedUp, Is.True);
        }
    }
}