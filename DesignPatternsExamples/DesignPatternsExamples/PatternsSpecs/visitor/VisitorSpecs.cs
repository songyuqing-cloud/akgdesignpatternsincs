using NUnit.Framework;
using Patterns.visitor;

namespace PatternsSpecs.visitor {
    
    [TestFixture]
    public class VisitorSpecs {

        [Test]
        public void generalOperationsCanVisitGeneralWidgets() {
            Widget widget0 = new WidgetA {justAnInt = 42};
            Widget widget1 = new WidgetB {justADouble = 666.6};

            Operation operation0 = new ShortContentToStringOperation();
            Operation operation1 = new LongContentToStringOperation();

            Assert.That(operation0.executeOn(widget0), Is.EqualTo("42"));
            Assert.That(operation0.executeOn(widget1), Is.EqualTo("666.6"));
            
            Assert.That(operation1.executeOn(widget0), Is.EqualTo("WidgetA[42]"));
            Assert.That(operation1.executeOn(widget1), Is.EqualTo("WidgetB[666.6]"));
        }
    }
    
}