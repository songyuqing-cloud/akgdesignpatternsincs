using NUnit.Framework;
using Patterns.parametrizedfactory;
using Patterns.prototype;

namespace PatternsSpecs.parametrizedfactory {
    [TestFixture]
    public class ParametrizedFactorySpecs {

        [Test]
        public void showParametrizedFactory() {
            // somewhere in our code we must build the factory and register all supported prototypes
            var factory = new ParametrizedFactoryBasedOnPrototypes();
            factory.registerPrototype(Create.WidgetA, new WidgetA(21));
            factory.registerPrototype(Create.WidgetB, new WidgetB(321.0));
            
            // and somewhere else it is used to create objects
            var obj0 = factory.create(Create.WidgetA);
            Assert.That((obj0 as WidgetA).justAValue, Is.EqualTo(21));
            var obj1 = factory.create(Create.WidgetB);
            Assert.That((obj1 as WidgetB).justAValue, Is.EqualTo(321.0));
        }
    }
}