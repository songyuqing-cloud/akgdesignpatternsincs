using NUnit.Framework;
using Patterns.prototype;

namespace PatternsSpecs.prototype {
    [TestFixture]
    public class PrototypeSpecs {

        public Widget doSomethingWhichNeedsToCreateObjects(Widget prototype) {
            var newWidget = prototype.createDuplicate();
            // do something with the new widget...
            return newWidget;
        }
        
        [Test]
        public void showCreationThroughPrototypes() {
            var prototype0 = new WidgetA(42);
            var prototype1 = new WidgetB(666.0);

            Assert.That((doSomethingWhichNeedsToCreateObjects(prototype0) as WidgetA).justAValue, Is.EqualTo(42));
            Assert.That((doSomethingWhichNeedsToCreateObjects(prototype1) as WidgetB).justAValue, Is.EqualTo(666.0));
        }
    }
}