using System;
using NUnit.Framework;
using Patterns.physicalunits;

namespace PatternsSpecs.physicalunits {
    [TestFixture]
    public class PhysicalUnitsSpecs {

        [Test]
        public void physicalUnitDemo() {
            Assert.That(PhysicalUnit.fahrenheit.isOfQuantity(Quantity.Temperature), Is.True);
            Assert.That(Quantity.Mass.baseUnit.isOfQuantity(Quantity.Force), Is.False);

            var fromInchToCentimeter = PhysicalUnit.inch.conversionMethodTo(PhysicalUnit.centimeter);
            var fromCelsiusToFahrenheit = PhysicalUnit.celsius.conversionMethodTo(PhysicalUnit.fahrenheit);
            var fromCubicDecimeterToLiter = PhysicalUnit.decimeter.pow(3).conversionMethodTo(PhysicalUnit.liter);
            var fromDecimeterToCentimeter = PhysicalUnit.decimeter.conversionMethodTo(PhysicalUnit.centimeter);
            
            Assert.That(new Action(() => PhysicalUnit.meter.conversionMethodTo(PhysicalUnit.ampere)),
                Throws.InstanceOf<ArgumentException>());
            Assert.That(fromInchToCentimeter(2.0), Is.EqualTo(5.08));
            Assert.That(fromCelsiusToFahrenheit(25.0), Is.EqualTo(77.0));
            Assert.That(fromCubicDecimeterToLiter(1.0), Is.EqualTo(1.0));
            Assert.That(fromDecimeterToCentimeter(2.0), Is.EqualTo(20.0));
        }
    }
}