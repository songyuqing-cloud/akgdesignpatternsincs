using System;
using NUnit.Framework;
using Patterns.fetcher;

namespace PatternsSpecs.fetcher {
    [TestFixture]
    public class WidgetUtilizingFetchersSpecs {

        [Test]
        public void thisIsHowTheFetchersWorkInPrinciple() {
            var target = new WidgetUtilizingFetchers();
            string output = null;
            
            var callCount = 0;
            target.fetchPrefixAs(() => "call-count: " + (++callCount) + " -> ");
            target.fetchSuffixAs(() => " ... sys-time: " + DateTime.Now);
            
            Assert.That(target.generateCompleteString("XXX").StartsWith("call-count: 1 -> XXX ... sys-time"), Is.True);
        }
        
        [Test]
        public void thisBecomesAProblem() {
            var target = new WidgetUtilizingFetchers();
           
            Func<string> badFetcher = () => "prefix 0";
            badFetcher += () => "prefix 1";
            badFetcher += () => "prefix 2";
            
            target.fetchPrefixAs(badFetcher); // and which one comes first now?
            target.fetchSuffixAs(() => "suffix");

            var obtained = target.generateCompleteString("XXX");
            
            // Assert.That(obtained.StartsWith("prefix 0"), Is.True);
            // Assert.That(obtained.StartsWith("prefix 1"), Is.True);
            Assert.That(obtained.StartsWith("prefix 2"), Is.True); // on my machine this is it...
        }
    }
}