using NUnit.Framework;
using Patterns.factorymethod;

namespace PatternsSpecs.factorymethod {
    [TestFixture]
    public class FactoryMethodSpecs {

        [Test]
        public void combinationsCanBeCalculated() {
            var widgetA0 = new WidgetA(17);
            var widgetA1 = new WidgetA(4);
            var widgetB0 = new WidgetB(170);
            var widgetB1 = new WidgetB(40);
            
            Assert.That((widgetA0.combinedWith(widgetA1) is WidgetA), Is.True);
            Assert.That(widgetA0.combinedWith(widgetA1).value, Is.EqualTo(21));
            
            Assert.That((widgetA0.combinedWith(widgetB0) is WidgetA), Is.True);
            Assert.That(widgetA0.combinedWith(widgetB0).value, Is.EqualTo(187));
            
            Assert.That((widgetB0.combinedWith(widgetB1) is WidgetB), Is.True);
            Assert.That(widgetB0.combinedWith(widgetB1).value, Is.EqualTo(210));
            
            Assert.That((widgetB0.combinedWith(widgetA1) is WidgetB), Is.True);
            Assert.That(widgetB0.combinedWith(widgetA1).value, Is.EqualTo(174));
        }
    }
}