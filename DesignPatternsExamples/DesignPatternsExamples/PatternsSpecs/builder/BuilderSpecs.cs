using System.Collections.Generic;
using NUnit.Framework;
using static Patterns.builder.QueryLanguage;

namespace PatternsSpecs.builder {
    [TestFixture]
    public class BuilderSpecs {
        
        [Test]
        public void builderQuery() {
            var src = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

            var queryWalker = src.walker().skip(2).and().takeOnly(value => value % 2 == 0);

            var expected = 4;
            while (queryWalker.hasCurrent) {
                Assert.That(queryWalker.currentValue, Is.EqualTo(expected));
                expected += 2;
                queryWalker.walkToNext();
            }
        }
    }
}