using System.Collections.Generic;
using NUnit.Framework;
using Patterns.walker;

namespace PatternsSpecs.walker {
    [TestFixture]
    public class WalkerSpecs {

        [Test]
        public void listWalkersCanWalkThroughLists() {
            var src = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            var target0 = new ListWalker<int>(src);
            var target1 = new ListWalker<int>(src);

            Assert.That(target0.hasCurrent, Is.True);
            Assert.That(target1.hasCurrent, Is.True);
            
            Assert.That(target1.walkToNext(), Is.True);
            Assert.That(target0.currentValue, Is.EqualTo(1));
            Assert.That(target1.currentValue, Is.EqualTo(2));
            
            Assert.That(target0.walkTo(5), Is.True);
            Assert.That(target0.currentValue, Is.EqualTo(6));
            Assert.That(target1.currentValue, Is.EqualTo(2));
            
            Assert.That(target1.walkToLast(), Is.True);
            Assert.That(target0.currentValue, Is.EqualTo(6));
            Assert.That(target1.currentValue, Is.EqualTo(10));
        }
       
        [Test]
        public void filterWalkersSelectCertainElements() {
            var src = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            var listWalker = new ListWalker<int>(src);
            var target = new FilterWalker<int>(listWalker, value => value % 2 == 0);

            var expectedValue = 2;

            while (target.hasCurrent) {
                Assert.That(target.currentValue, Is.EqualTo(expectedValue));
                target.walkToNext();
                expectedValue += 2;
            }
        }
        
        [Test]
        public void skipWalkersSkipAGivenNumberOfElementsAtTheBeginning() {
            var src = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            var listWalker = new ListWalker<int>(src);
            var target = new SkipWalker<int>(listWalker, 3);

            var expectedValue = 4;

            while (target.hasCurrent) {
                Assert.That(target.currentValue, Is.EqualTo(expectedValue));
                target.walkToNext();
                ++expectedValue;
            }
        }
        
        [Test]
        public void hasNextStyleWalkerCanBeCreatedFromAStandardWalker() {
            var src = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            var listWalker = new ListWalker<int>(src);
            var target = new HasNextStyleWalker<int>(listWalker);

            var expectedValue = 1;

            while (target.hasNext()) {
                Assert.That(target.next, Is.EqualTo(expectedValue));
                ++expectedValue;
            }
        }
        
    }
}