using System;
using NUnit.Framework;
using Patterns.command;

namespace PatternsSpecs.command {
    [TestFixture]
    public class WidgetForcingSingleCommandsSpecs {
        [Test]
        public void doSomethingOutputsTheMessageTheWayItWasSetBefore() {
            var target = new WidgetForcingSingleCommands();

            string notification = null;
            target.setCmdToShowNotification(msg => notification = msg);
            
            target.doSomething();
            Assert.That(notification, Is.EqualTo("performed doSomething"));
        }
        
        [Test]
        public void doWeReallyWantMoreThanOneFuncToBeCalled() {
            var target = new WidgetForcingSingleCommands();

            string notification = null;
            string otherNotification = null;
            Action<string> cmds = msg => notification = msg;
            cmds += msg => otherNotification = msg;
            target.setCmdToShowNotification(cmds);
            
            target.doSomething();
            Assert.That(notification, Is.EqualTo("performed doSomething"));
            Assert.That(otherNotification, Is.EqualTo("performed doSomething"));
        }
    }
}