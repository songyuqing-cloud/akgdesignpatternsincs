using NUnit.Framework;
using Patterns.command;

namespace PatternsSpecs.command {
    [TestFixture]
    public class WidgetUtilizingCommandsSpecs {
        [Test]
        public void doSomethingOutputsTheMessageTheWayItWasSetBefore() {
            var target = new WidgetUtilizingCommands();

            string notification = null;
            target.cmdToShowNotification = msg => notification = msg;
            
            target.doSomething();
            Assert.That(notification, Is.EqualTo("performed doSomething"));
        }
        
        [Test]
        public void doWeReallyWantMoreThanOneFuncToBeCalled() {
            var target = new WidgetUtilizingCommands();

            string notification = null;
            target.cmdToShowNotification = msg => notification = msg;
            
            string otherNotification = null;
            target.cmdToShowNotification += msg => otherNotification = msg;
            
            target.doSomething();
            Assert.That(notification, Is.EqualTo("performed doSomething"));
            Assert.That(otherNotification, Is.EqualTo("performed doSomething"));
        }
    }
}