using NUnit.Framework;
using Patterns.singleton;

namespace PatternsSpecs.singleton {
    [TestFixture]
    public class SingletonSpecs {

        [Test]
        public void showLogger() {
            MyLogger.instance.log("this is a log entry");
        }
    }
}