using NUnit.Framework;
using Patterns.bridge;

namespace PatternsSpecs.bridge {
  
    [TestFixture]
    public class BridgeSpecs {
        
        class Widget {
            public string name;
        }

        [Test]
        public void lazyObjectsAreCreatedJustInTime() {
            var target = new Lazy<Widget>(() => new Widget {name = "A"});
            
            Assert.That(target.isCreated, Is.False);

            var tmp = target.value;
            Assert.That(target.isCreated, Is.True);
            Assert.That(target.value.name, Is.EqualTo("A"));
            
            target.value.name = "changed";
            Assert.That(target.value.name, Is.EqualTo("changed"));
        }
        
        [Test]
        public void copyOnWriteCanBeImplementedAsABridge() {
            var target = new WidgetDoingCopyOnWrite();
            target.expensiveValue = 42;
            
            Assert.That(target.refCount, Is.EqualTo(1));
            Assert.That(target.doSomething(), Is.EqualTo(84));

            var clone0 = target.getClone();
            var clone1 = target.getClone();
            Assert.That(target.refCount, Is.EqualTo(3));
            Assert.That(clone0.refCount, Is.EqualTo(3));
            Assert.That(clone1.refCount, Is.EqualTo(3));
            
            Assert.That(target.doSomething(), Is.EqualTo(84));
            Assert.That(clone0.doSomething(), Is.EqualTo(84));
            Assert.That(clone1.doSomething(), Is.EqualTo(84));

            target.expensiveValue = 43;
            Assert.That(target.refCount, Is.EqualTo(1));
            Assert.That(clone0.refCount, Is.EqualTo(2));
            Assert.That(clone1.refCount, Is.EqualTo(2));
            
            Assert.That(target.doSomething(), Is.EqualTo(86));
            Assert.That(clone0.doSomething(), Is.EqualTo(84));
            Assert.That(clone1.doSomething(), Is.EqualTo(84));
        }
        
    }
    
}