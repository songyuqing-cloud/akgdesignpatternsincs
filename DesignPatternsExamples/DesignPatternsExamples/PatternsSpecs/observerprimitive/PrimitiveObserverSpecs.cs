using NUnit.Framework;
using Patterns.observerprimitive;

namespace PatternsSpecs.observerprimitive {
    
    [TestFixture]
    public class PrimitiveObserverSpecs {
        [Test]
        public void showPrimitiveObserver() {
            var widget0 = new ObservableWidget("0");
            var widget1 = new ObservableWidget("1");
            
            var observer0 = new WidgetObserver();
            var observer1 = new WidgetObserver();
            
            widget0.addObserver(observer0);
            widget1.addObserver(observer1);

            widget0.justAValue = 42;
            widget1.justAValue = 43;
            widget0.justAValue = 44;
            widget1.justAValue = 45;
            
            Assert.That(observer0.changeNotifications[0], Is.EqualTo(42));
            Assert.That(observer0.changeNotifications[1], Is.EqualTo(44));
            
            Assert.That(observer1.changeNotifications[0], Is.EqualTo(43));
            Assert.That(observer1.changeNotifications[1], Is.EqualTo(45));
        }
    }
    
}