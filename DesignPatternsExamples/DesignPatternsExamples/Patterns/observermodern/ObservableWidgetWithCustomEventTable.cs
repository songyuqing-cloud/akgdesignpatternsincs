using System;
using System.Collections.Generic;

namespace Patterns.observermodern {
    
    public class ObservableWidgetWithCustomEventTable {
        public ObservableWidgetWithCustomEventTable(string name) { this.name = name; }
        public string name { get; }

        private int justAValueField;
        public int justAValue {
            get { return justAValueField; }
            set {
                var oldValue = justAValueField;
                justAValueField = value;
                Action<ObservableWidgetWithCustomEventTable, int, int> registeredHandlers;
                if (!observers.TryGetValue(justAValueChangedKey, out registeredHandlers))
                    return;
                registeredHandlers(this, oldValue, justAValue);
            }
        }

        private readonly Dictionary<object, Action<ObservableWidgetWithCustomEventTable, int, int>> observers =
            new Dictionary<object, Action<ObservableWidgetWithCustomEventTable, int, int>>();
        private static readonly object justAValueChangedKey = new object();
        public event Action<ObservableWidgetWithCustomEventTable, int, int> justAValueChanged {
            add {
                if (observers.ContainsKey(justAValueChangedKey))
                    observers[justAValueChangedKey] += value;
                else
                    observers[justAValueChangedKey] = value;
            }
            remove {
                Action<ObservableWidgetWithCustomEventTable, int, int> registeredHandlers;
                if (!observers.TryGetValue(justAValueChangedKey, out registeredHandlers))
                    return;
                registeredHandlers -= value;
                observers[justAValueChangedKey] = registeredHandlers;
            }
        }
    }
    
}