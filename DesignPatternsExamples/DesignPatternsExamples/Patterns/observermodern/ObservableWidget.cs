using System;

namespace Patterns.observermodern {
    
    public class ObservableWidget {
        public ObservableWidget(string name) { this.name = name; }
        public string name { get; }

        private int justAValueField;
        public int justAValue {
            get { return justAValueField; }
            set {
                var oldValue = justAValueField;
                justAValueField = value;
                var observers = justAValueChanged; // make tmp-copy... we want solid code!
                observers?.Invoke(this, oldValue, justAValue);
            }
        }

        public event Action<ObservableWidget, int, int> justAValueChanged;
    }
    
}