
using System;

namespace Patterns.encapsulatedcalls {
    public static class EncapsulatedCalls {
        
        public static int orIfEqual(this int lastComparisonResult, Func<int> nextInChain) {
            return lastComparisonResult != 0 ? lastComparisonResult : nextInChain();
        }

        public static TValue modifiedAs<TValue>(this TValue value, Action<TValue> modifyIt) {
            return value.afterDoing(modifyIt);
        }
        
        public static TValue afterDoing<TValue>(this TValue value, Action<TValue> doSomethingWithIt) {
            doSomethingWithIt(value);
            return value;
        }
        
        public static TValue afterDoing<TValue>(this TValue value, Action doSomething) {
            doSomething();
            return value;
        }

        public static TResult doWithCleanup<TResult>(Func<TResult> doThat, Action cleanup) {
            try { return doThat(); }
            finally { cleanup(); }
        }
    }
}