
namespace Patterns.physicalunits {
    public class Quantity {
        public readonly PhysicalUnit baseUnit;

        public Quantity(PhysicalUnit baseUnit) { this.baseUnit = baseUnit; }
        public override string ToString() {
            return "Quantity{ kg = " + baseUnit.exponentOf(PhysicalUnit.ExponentIdx.Kilogram) +
                   ", m = " + baseUnit.exponentOf(PhysicalUnit.ExponentIdx.Meter) +
                   ", s = " + baseUnit.exponentOf(PhysicalUnit.ExponentIdx.Second) +
                   ", A = " + baseUnit.exponentOf(PhysicalUnit.ExponentIdx.Ampere) +
                   ", K = " + baseUnit.exponentOf(PhysicalUnit.ExponentIdx.Kelvin) +
                   ", cd = " + baseUnit.exponentOf(PhysicalUnit.ExponentIdx.Candela) +
                   ", mol = " + baseUnit.exponentOf(PhysicalUnit.ExponentIdx.Mole) + " }";
        }

        public static readonly Quantity Scalar = new Quantity(PhysicalUnit.scalar);
        public static readonly Quantity Mass = new Quantity(PhysicalUnit.kilogram);
        public static readonly Quantity Length = new Quantity(PhysicalUnit.meter);
        public static readonly Quantity Time = new Quantity(PhysicalUnit.second);
        public static readonly Quantity Current = new Quantity(PhysicalUnit.ampere);
        public static readonly Quantity Temperature = new Quantity(PhysicalUnit.kelvin);
        public static readonly Quantity LuminousIntentity = new Quantity(PhysicalUnit.candela);
        public static readonly Quantity AmountOfSubstance = new Quantity(PhysicalUnit.mole);
        public static readonly Quantity Speed = new Quantity(PhysicalUnit.metersPerSecond);
        public static readonly Quantity Acceleration = new Quantity(PhysicalUnit.metersPerSquareSecond);
        public static readonly Quantity Area = new Quantity(PhysicalUnit.squareMeter);
        public static readonly Quantity Volume = new Quantity(PhysicalUnit.cubicMeter);
        public static readonly Quantity Force = new Quantity(PhysicalUnit.newton);
        // ... further quantities go here...
    }
}