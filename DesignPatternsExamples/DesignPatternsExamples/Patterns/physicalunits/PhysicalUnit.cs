using System;

namespace Patterns.physicalunits {
    public class PhysicalUnit {
        private int[] exponents;
        public double scalarFactor { get; }
        public double zeroOffset { get; }

        private PhysicalUnit(int[] exponents, double scalarFactor, double zeroOffset) {
            this.exponents = exponents; // we can reference them as the object is immutable
            this.scalarFactor = scalarFactor;
            this.zeroOffset = zeroOffset;
        }
        
        public enum ExponentIdx { Kilogram, Meter, Second, Ampere, Kelvin, Candela, Mole }
        public bool isCoherent { get { return scalarFactor == 1.0 && zeroOffset == 0.0; } }
        public PhysicalUnit coherentBase { get { return new PhysicalUnit(exponents, 1.0, 0.0); } }

        public PhysicalUnit withZeroAt(double zeroOffset) {
            return new PhysicalUnit (exponents, scalarFactor, zeroOffset / scalarFactor); // the new offset is scaled according to the current scale of the unit
        }
        public int exponentOf(ExponentIdx what) { return exponents[(int)what]; }

        public bool isOfQuantity(Quantity quantity) { return areExponentsEqualTo(quantity.baseUnit.exponents); }
        public Func<double, double> conversionMethodTo(PhysicalUnit dst) { 
            if (!areExponentsEqualTo(dst.exponents))
                throw new ArgumentException("src and dst units are incompatible");
            return srcValue => (srcValue + zeroOffset) * (scalarFactor / dst.scalarFactor) - dst.zeroOffset;
        }

        public override string ToString() {
            return "PhysicalUnit{ [kg = " + exponents[(int)ExponentIdx.Kilogram] +
                   ", m = " + exponents[(int)ExponentIdx.Meter] +
                   ", s = " + exponents[(int)ExponentIdx.Second] +
                   ", A = " + exponents[(int)ExponentIdx.Ampere] +
                   ", K = " + exponents[(int)ExponentIdx.Kelvin] +
                   ", cd = " + exponents[(int)ExponentIdx.Candela] +
                   ", mol = " + exponents[(int)ExponentIdx.Mole] + 
                   "]" +
                   ", scalarFactor = " + scalarFactor +
                   ", zeroOffset = " + zeroOffset +
                   " }";
        }

        private bool areExponentsEqualTo(int[] rhsExponents) {
            return exponents[0] == rhsExponents[0] &&
                   exponents[1] == rhsExponents[1] &&
                   exponents[2] == rhsExponents[2] &&
                   exponents[3] == rhsExponents[3] &&
                   exponents[4] == rhsExponents[4] &&
                   exponents[5] == rhsExponents[5] &&
                   exponents[6] == rhsExponents[6];
        }
        public override bool Equals(object obj) {
            var other = obj as PhysicalUnit;
            if (other == null)
                return false;
            return scalarFactor == other.scalarFactor &&
                   zeroOffset == other.zeroOffset &&
                   areExponentsEqualTo(other.exponents);
        }

        public override int GetHashCode() {
            var hashCode = scalarFactor.GetHashCode() * 31 + zeroOffset.GetHashCode();
            hashCode = hashCode * 31 + exponents[0].GetHashCode();
            hashCode = hashCode * 31 + exponents[1].GetHashCode();
            hashCode = hashCode * 31 + exponents[2].GetHashCode();
            hashCode = hashCode * 31 + exponents[3].GetHashCode();
            hashCode = hashCode * 31 + exponents[4].GetHashCode();
            hashCode = hashCode * 31 + exponents[5].GetHashCode();
            hashCode = hashCode * 31 + exponents[6].GetHashCode();
            return hashCode;
        }

        public PhysicalUnit pow(int value) {
            return new PhysicalUnit(new [] {
                    exponents[0] * value,
                    exponents[1] * value,
                    exponents[2] * value,
                    exponents[3] * value,
                    exponents[4] * value,
                    exponents[5] * value,
                    exponents[6] * value
                }, Math.Pow(scalarFactor, value), 0.0);
        }
        
        public static PhysicalUnit operator * (PhysicalUnit lhs, double rhs) {
            return new PhysicalUnit(lhs.exponents, lhs.scalarFactor * rhs, 0.0);
        }
        public static PhysicalUnit operator * (double lhs, PhysicalUnit rhs) {
            return rhs * lhs;
        }
        public static PhysicalUnit operator * (PhysicalUnit lhs, PhysicalUnit rhs) {
            return new PhysicalUnit(new [] {
                    lhs.exponents[0] + rhs.exponents[0],
                    lhs.exponents[1] + rhs.exponents[1],
                    lhs.exponents[2] + rhs.exponents[2],
                    lhs.exponents[3] + rhs.exponents[3],
                    lhs.exponents[4] + rhs.exponents[4],
                    lhs.exponents[5] + rhs.exponents[5],
                    lhs.exponents[6] + rhs.exponents[6]
                }, lhs.scalarFactor * rhs.scalarFactor, 0.0);
        }
        
        public static PhysicalUnit operator / (PhysicalUnit lhs, double rhs) {
            return new PhysicalUnit(lhs.exponents, lhs.scalarFactor / rhs, 0.0);
        }
        public static PhysicalUnit operator / (double lhs, PhysicalUnit rhs) {
            return new PhysicalUnit(new [] {
                    -rhs.exponents[0],
                    -rhs.exponents[1],
                    -rhs.exponents[2],
                    -rhs.exponents[3],
                    -rhs.exponents[4],
                    -rhs.exponents[5],
                    -rhs.exponents[6]
                }, lhs / rhs.scalarFactor, 0.0);
        }
        public static PhysicalUnit operator / (PhysicalUnit lhs, PhysicalUnit rhs) {
            return new PhysicalUnit(new [] {
                    lhs.exponents[0] - rhs.exponents[0],
                    lhs.exponents[1] - rhs.exponents[1],
                    lhs.exponents[2] - rhs.exponents[2],
                    lhs.exponents[3] - rhs.exponents[3],
                    lhs.exponents[4] - rhs.exponents[4],
                    lhs.exponents[5] - rhs.exponents[5],
                    lhs.exponents[6] - rhs.exponents[6]
                }, lhs.scalarFactor / rhs.scalarFactor, 0.0);
        }
        
        // SI base units
        public static PhysicalUnit scalar = new PhysicalUnit(new []{ 0, 0, 0, 0, 0, 0, 0 }, 1.0, 0.0);
        public static PhysicalUnit kilogram = new PhysicalUnit(new []{ 1, 0, 0, 0, 0, 0, 0 }, 1.0, 0.0);
        public static PhysicalUnit meter = new PhysicalUnit(new []{ 0, 1, 0, 0, 0, 0, 0 }, 1.0, 0.0);
        public static PhysicalUnit second = new PhysicalUnit(new []{ 0, 0, 1, 0, 0, 0, 0 }, 1.0, 0.0);
        public static PhysicalUnit ampere = new PhysicalUnit(new []{ 0, 0, 0, 1, 0, 0, 0 }, 1.0, 0.0);
        public static PhysicalUnit kelvin = new PhysicalUnit(new []{ 0, 0, 0, 0, 1, 0, 0 }, 1.0, 0.0);
        public static PhysicalUnit candela = new PhysicalUnit(new []{ 0, 0, 0, 0, 0, 1, 0 }, 1.0, 0.0);
        public static PhysicalUnit mole = new PhysicalUnit(new []{ 0, 0, 0, 0, 0, 0, 1 }, 1.0, 0.0);
        
        // scalar modifiers
        public static PhysicalUnit yocto = 1e-24 * scalar;
        public static PhysicalUnit zepto = 1e-21 * scalar;
        public static PhysicalUnit atto = 1e-18 * scalar;
        public static PhysicalUnit femto = 1e-15 * scalar;
        public static PhysicalUnit pico = 1e-12 * scalar;
        public static PhysicalUnit nano = 1e-9 * scalar;
        public static PhysicalUnit micro = 1e-6 * scalar;
        public static PhysicalUnit milli = 1e-3 * scalar;
        public static PhysicalUnit centi = 1e-2 * scalar;
        public static PhysicalUnit deci = 1e-1 * scalar;

        public static PhysicalUnit deca = 1e1 * scalar;
        public static PhysicalUnit hecto = 1e2 * scalar;
        public static PhysicalUnit kilo = 1e3 * scalar;
        public static PhysicalUnit mega = 1e6 * scalar;
        public static PhysicalUnit giga = 1e9 * scalar;
        public static PhysicalUnit tera = 1e12 * scalar;
        public static PhysicalUnit peta = 1e15 * scalar;
        public static PhysicalUnit exa = 1e18 * scalar;
        public static PhysicalUnit zetta = 1e21 * scalar;
        public static PhysicalUnit yotta = 1e24 * scalar;

        // any non-SI or derived units
        public static PhysicalUnit centimeter = centi * meter;
        public static PhysicalUnit decimeter = deci * meter;
        public static PhysicalUnit inch = 2.54 * centimeter;
        public static PhysicalUnit metersPerSecond = meter / second;
        public static PhysicalUnit metersPerSquareSecond = meter / second.pow(2);
        public static PhysicalUnit squareMeter = meter.pow(2);
        public static PhysicalUnit cubicMeter = meter.pow(3);
        public static PhysicalUnit liter = decimeter.pow(3);
        public static PhysicalUnit milliLiter = milli * liter;
        public static PhysicalUnit celsius = kelvin.withZeroAt(273.15);
        public static PhysicalUnit fahrenheit = (kelvin * 5.0 / 9.0) // Important: first comes the scaling...
                                         .withZeroAt(459.67 * 5.0 / 9.0); // and then comes the offset in terms of the base, i.e. 255,37222222... K is 0F
        public static PhysicalUnit newton = kilogram * meter / second.pow(2);
    }
}