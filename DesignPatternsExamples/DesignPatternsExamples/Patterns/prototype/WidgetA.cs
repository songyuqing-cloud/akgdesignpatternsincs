namespace Patterns.prototype {
    public class WidgetA : Widget {
        public int justAValue { get; set; }

        public WidgetA(int justAValue) {
            this.justAValue = justAValue;
        }
        public Widget createDuplicate() {
            return new WidgetA(justAValue);
        }

        public override string ToString() {
            return "WidgetA[just a value = " + justAValue + "]";
        }
    }
}