namespace Patterns.prototype {
    public interface Widget {
        Widget createDuplicate();
    }
}