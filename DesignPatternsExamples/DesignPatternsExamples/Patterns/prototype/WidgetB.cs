namespace Patterns.prototype {
    public class WidgetB : Widget {
        public double justAValue { get; set; }

        public WidgetB(double justAValue) {
            this.justAValue = justAValue;
        }
        public Widget createDuplicate() {
            return new WidgetB(justAValue);
        }

        public override string ToString() {
            return "WidgetB[just a value = " + justAValue + "]";
        }
    }
}