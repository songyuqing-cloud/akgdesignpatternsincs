namespace Patterns.state {
    
    public enum Event {
        ShortPress,
        DoubleShortPress,
        TripleShortPress,
        LongPress,
        TimerDue
    }
    
    // State: Sleep-Mode
    //    Long Press -> Power-On
    // State: Power-On
    //    Long Press -> Sleep-Mode
    //    Short Press -> Prepare-Drink
    //    Double Short Press -> Programming-Mode
    // State: Prepare-Drink
    //    Short Press -> Power-On
    //    Timer due -> Power-On
    // State: Programming-Mode
    //    Long Press -> Sleep-Mode
    //    Triple Short Press -> Power-On
    //    ... other programming events ... we don't care :-)
    public class OneButtonInterfaceStateGraph {
        private State<Event> sleepState = new State<Event>();
        private State<Event> powerOnState = new State<Event>();
        private State<Event> prepareDrinkState = new State<Event>();
        private State<Event> programmingModeState = new State<Event>();
        
        private State<Event> currentState;
        
        public ReadonlyState currentStateRef { get { return currentState; } }
        public ReadonlyState sleepStateRef { get { return sleepState; } }
        public ReadonlyState powerOnStateRef { get { return powerOnState; } }
        public ReadonlyState prepareDrinkStateRef { get { return prepareDrinkState; } }
        public ReadonlyState programmingModeStateRef{ get { return programmingModeState; } }

        public OneButtonInterfaceStateGraph() {
            // build the state-graph by adding transitions
            sleepState.addTransition(Event.LongPress, powerOnState, () => { /* transition-action here */ });
            
            powerOnState.addTransition(Event.LongPress, sleepState, () => { /* transition-action here */ });
            powerOnState.addTransition(Event.ShortPress, prepareDrinkState, () => { /* transition-action here */ });
            powerOnState.addTransition(Event.DoubleShortPress, programmingModeState, () => { /* transition-action here */ });
            
            prepareDrinkState.addTransition(Event.ShortPress, powerOnState, () => { /* transition-action here */ });
            prepareDrinkState.addTransition(Event.TimerDue, powerOnState, () => { /* transition-action here */ });
            
            programmingModeState.addTransition(Event.LongPress, sleepState, () => { /* transition-action here */ });
            programmingModeState.addTransition(Event.TripleShortPress, powerOnState, () => { /* transition-action here */ });

            currentState = sleepState; // don't forget to set the starting-state
        }

        public void receivedEvent(Event evt) {
            currentState = currentState.onEvent(evt);
        }
    }
}