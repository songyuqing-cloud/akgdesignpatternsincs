using System;

namespace Patterns.state {
    public class OneButtonInterfaceFunctionRepresentsState {
        private Action<Event> onEvent;

        public OneButtonInterfaceFunctionRepresentsState() {
           onEvent = eventReceivedInSleepState;
        }
        
        public void receivedEvent(Event evt) { onEvent(evt); }

        private void eventReceivedInSleepState(Event evt) {
            if (evt == Event.LongPress) { // transition to Power-On
                // ** perform transition-action here
                onEvent = eventReceivedInPowerOnState;
            }
        }

        private void eventReceivedInPowerOnState(Event evt) {
            switch(evt) {
                case Event.LongPress:
                    // ** perform transition-action here
                    onEvent = eventReceivedInSleepState;
                    break;
                case Event.ShortPress:
                    // ** perform transition-action here
                    onEvent = eventReceivedInPrepareDrinkState;
                    break;
                case Event.DoubleShortPress:
                    // ** perform transition-action here
                    onEvent = eventReceivedInProgrammingModeState;
                    break;
                default:
                    break;
            }
        }

        private void eventReceivedInPrepareDrinkState(Event evt) {
            switch(evt) {
                case Event.ShortPress:
                case Event.TimerDue:
                    // ** perform transition-action here
                    onEvent = eventReceivedInPowerOnState;
                    break;
                default:
                    break;
            }
        }

        private void eventReceivedInProgrammingModeState(Event evt) {
            switch(evt) {
                case Event.LongPress:
                    // ** perform transition-action here
                    onEvent = eventReceivedInSleepState;
                    break;
                case Event.TripleShortPress:
                    // ** perform transition-action here
                    onEvent = eventReceivedInPowerOnState;
                    break;
                default:
                    break;
            }

        }
    }
}