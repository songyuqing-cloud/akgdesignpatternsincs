using System;

namespace Patterns.state {
    
    public class Transition<TEvent> {
        private State<TEvent> dst;
        private Action doThat;

        public Transition(State<TEvent> dst, Action doThat) {
            this.dst = dst;
            this.doThat = doThat;
        }

        public State<TEvent> performTransition() {
            doThat();
            return dst;
        }

    }
}