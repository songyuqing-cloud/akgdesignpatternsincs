using System;
using System.Collections.Generic;

namespace Patterns.state {

    public interface ReadonlyState { } // just need it as a marker-interface for testing
    
    public class State<TEvent> : ReadonlyState {
        private readonly Dictionary<TEvent, Transition<TEvent>> transitions = new Dictionary<TEvent, Transition<TEvent>>();

        public void addTransition(TEvent evt, State<TEvent> dst, Action doThatOnTransition) {
            transitions.Add(evt, new Transition<TEvent>(dst, doThatOnTransition));
        }

        public State<TEvent> onEvent(TEvent evt) {
            Transition<TEvent> transition;
            if (!transitions.TryGetValue(evt, out transition))
                return this;
            return transition.performTransition();
        }

    }
}