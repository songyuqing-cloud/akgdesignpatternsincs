using System;
using System.Collections.Generic;
using Patterns.walker;

namespace Patterns.builder {
    
    public static class QueryLanguage {
        public static Walker<TValue> walker<TValue>(this List<TValue> src) {
            return new ListWalker<TValue>(src);
        }
        
        public static Walker<TValue> takeOnly<TValue>(this Walker<TValue> src, Func<TValue, bool> takeIt) {
            return new FilterWalker<TValue>(src, takeIt);
        }
        
        public static Walker<TValue> skip<TValue>(this Walker<TValue> src, int numElementsToSkip) {
            return new SkipWalker<TValue>(src, numElementsToSkip);
        }
        
        public static Walker<TValue> and<TValue>(this Walker<TValue> src) {
            return src;
        }
    }
}