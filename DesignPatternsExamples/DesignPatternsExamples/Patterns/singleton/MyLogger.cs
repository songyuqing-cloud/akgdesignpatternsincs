using Patterns.bridge;

namespace Patterns.singleton {
    public class MyLogger {
        public static MyLogger instance {
            get { return singleInstance.value; }
        }

        public void log(string msg) {
            // perform logging here...
        }

        private MyLogger() { }
        private static Lazy<MyLogger> singleInstance = new Lazy<MyLogger>(() => new MyLogger());
    }
}