namespace Patterns.factorymethod {
    public class WidgetA : WidgetBase {
        public WidgetA(int value) : base(value) { }

        public WidgetA combinedWith(WidgetA other) {
            return (WidgetA)calculateWidgetCombinedWith(other, value => new WidgetA(value));
        }
        
        public WidgetA combinedWith(WidgetB other) {
            return (WidgetA)calculateWidgetCombinedWith(other, value => new WidgetA(value));
        }
    }
}