using System;

namespace Patterns.factorymethod {
    public class WidgetBase {
        public int value { get; }

        protected WidgetBase(int value) {
            this.value = value;
        }

        protected WidgetBase calculateWidgetCombinedWith(WidgetBase other, Func<int, WidgetBase> createIt) {
            return createIt(value + other.value);
        }
    }
}