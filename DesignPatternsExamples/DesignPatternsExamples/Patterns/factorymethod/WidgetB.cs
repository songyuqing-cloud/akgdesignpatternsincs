namespace Patterns.factorymethod {
    public class WidgetB : WidgetBase {
        public WidgetB(int value) : base(value) { }

        public WidgetB combinedWith(WidgetA other) {
            return (WidgetB)calculateWidgetCombinedWith(other, value => new WidgetB(value));
        }
        
        public WidgetB combinedWith(WidgetB other) {
            return (WidgetB)calculateWidgetCombinedWith(other, value => new WidgetB(value));
        }
        
    }
}