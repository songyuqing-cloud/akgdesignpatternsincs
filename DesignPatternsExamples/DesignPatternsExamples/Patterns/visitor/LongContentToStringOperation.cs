namespace Patterns.visitor {
    public class LongContentToStringOperation : Operation {
        public override string performSpecializedOn(WidgetA widget) {
            return "WidgetA[" + widget.justAnInt + "]";
        }

        public override string performSpecializedOn(WidgetB widget) {
            return "WidgetB[" + widget.justADouble + "]";
        }
    }
}