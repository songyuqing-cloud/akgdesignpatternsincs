namespace Patterns.visitor {
    public abstract class Operation {
        public string executeOn(Widget widget) {
            return widget.bounceBack(this);
        }

        public abstract string performSpecializedOn(WidgetA widget);
        public abstract string performSpecializedOn(WidgetB widget);
    }
}