namespace Patterns.visitor {
    public class WidgetB : Widget {
        public double justADouble = 0.0;
        
        public string bounceBack(Operation operation) {
            return operation.performSpecializedOn(this);
        }
    }
}