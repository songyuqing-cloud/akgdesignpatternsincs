namespace Patterns.visitor {
    public class ShortContentToStringOperation : Operation {
        public override string performSpecializedOn(WidgetA widget) {
            return widget.justAnInt.ToString();
        }

        public override string performSpecializedOn(WidgetB widget) {
            return widget.justADouble.ToString();
        }
    }
}