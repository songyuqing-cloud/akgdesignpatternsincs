namespace Patterns.visitor {
    public interface Widget {
        string bounceBack(Operation operation);
    }
}