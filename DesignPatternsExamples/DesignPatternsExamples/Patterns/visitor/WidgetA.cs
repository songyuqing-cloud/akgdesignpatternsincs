namespace Patterns.visitor {
    public class WidgetA : Widget {
        public int justAnInt = 0;
        
        public string bounceBack(Operation operation) {
            return operation.performSpecializedOn(this);
        }
    }
}