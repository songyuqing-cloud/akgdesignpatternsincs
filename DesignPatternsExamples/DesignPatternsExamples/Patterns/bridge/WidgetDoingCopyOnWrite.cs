namespace Patterns.bridge {

    class WidgetFunctionImpl {
        internal int refCount = 0;
        // the following object is very expensive to construct :-)
        internal int expensiveToConstruct = 0;

        // all the object's functionality is implemented here in the Impl-class
        internal int doSomething() {
            return expensiveToConstruct * 2;
        }
    }
    
    public class WidgetDoingCopyOnWrite {
        private WidgetFunctionImpl impl;
        
        public WidgetDoingCopyOnWrite() { impl = new WidgetFunctionImpl { refCount = 1 }; }

        private WidgetDoingCopyOnWrite(WidgetFunctionImpl impl) {
            this.impl = impl;
            ++this.impl.refCount;
        }

        ~WidgetDoingCopyOnWrite() {
            --impl.refCount;
        }

        public WidgetDoingCopyOnWrite getClone() {
            return new WidgetDoingCopyOnWrite(impl);
        }

        public int doSomething() { return impl.doSomething(); }

        public int expensiveValue {
            get { return impl.expensiveToConstruct; }
            set {
                if (impl.refCount > 1) {
                    --impl.refCount;
                    impl = new WidgetFunctionImpl { refCount = 1 };
                }
                impl.expensiveToConstruct = value;
            }
        }
        
        public int refCount { get { return impl.refCount; } }
    }
    
}