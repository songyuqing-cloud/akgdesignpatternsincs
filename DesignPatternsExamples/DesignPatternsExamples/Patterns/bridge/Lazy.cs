using System;

namespace Patterns.bridge {
    
    public class Lazy<TObj> where TObj : class {
        private TObj valueIfAvailable;
        private Func<TObj> createObj;

        public Lazy(Func<TObj> createObj) { this.createObj = createObj; }
        public bool isCreated { get { return valueIfAvailable != null; } }

        public TObj value {
            get {
                if (!isCreated)
                    valueIfAvailable = createObj();
                return valueIfAvailable;
            }
        }
    }
    
}