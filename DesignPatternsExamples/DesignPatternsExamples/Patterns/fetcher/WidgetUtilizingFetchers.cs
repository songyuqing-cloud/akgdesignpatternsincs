using System;

namespace Patterns.fetcher {
    public class WidgetUtilizingFetchers {
        private Func<string> getPrefix = () => "";
        private Func<string> getSuffix = () => "";

        public void fetchPrefixAs(Func<string> doThat) {
            if (doThat == null) throw new ArgumentException("doThat must not be null");
            getPrefix = doThat;
        }
        
        public void fetchSuffixAs(Func<string> doThat) {
            if (doThat == null) throw new ArgumentException("doThat must not be null");
            getSuffix = doThat;
        }

        public string generateCompleteString(string that) {
            return getPrefix() + that + getSuffix();
        } 
    }
}