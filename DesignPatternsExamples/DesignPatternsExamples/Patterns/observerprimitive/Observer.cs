namespace Patterns.observerprimitive {
    
    public interface Observer {
        void observableChanged(Observable observable);
    }
    
}