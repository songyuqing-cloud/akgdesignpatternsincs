using System.Collections.Generic;

namespace Patterns.observerprimitive {
    
    public class Observable {
        private List<Observer> observers = new List<Observer>();

        protected void notifyObservers() {
            foreach (var observer in observers) 
                observer.observableChanged(this);
        }

        public void addObserver(Observer observer) {
            observers.Add(observer);
        }

        public void removeObserver(Observer observer) {
            observers.Remove(observer);
        }
    }
}