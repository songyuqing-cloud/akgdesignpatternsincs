using System.Collections.Generic;

namespace Patterns.observerprimitive {
    
    public class WidgetObserver : Observer {
        public List<int> changeNotifications = new List<int>();
        
        public void observableChanged(Observable observable) {
            changeNotifications.Add((observable as ObservableWidget)?.justAValue ?? int.MinValue);
        }
        
    }
}