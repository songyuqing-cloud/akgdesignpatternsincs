namespace Patterns.observerprimitive {
   
    public class ObservableWidget : Observable {
        public ObservableWidget(string name) { this.name = name; }
        public string name { get; }

        private int justAValueField;
        public int justAValue {
            get { return justAValueField; }
            set {
                justAValueField = value;
                notifyObservers();
            }
        }
    }
    
}