namespace Patterns.proxy {
    public interface WidgetIfc {
        void doSomething();
    }
}