namespace Patterns.proxy {
    public interface ExpensivelyCalculatedValue<TValue> {
        TValue calculate();
    }
}