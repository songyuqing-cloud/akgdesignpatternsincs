using System.Diagnostics;

namespace Patterns.proxy {
    public class ClassNeedingWidget {
        private WidgetIfc widget;

        public ClassNeedingWidget(WidgetIfc widget) {
            this.widget = widget;
        }

        public long jumpFromTheCliff() {
            var stopwatch = Stopwatch.StartNew();
            widget.doSomething();
            return stopwatch.ElapsedMilliseconds;
        }
    }
}