using System.Threading.Tasks;

namespace Patterns.proxy {
    public class ExpensivelyCalculatedValueProxy<TValue> : ExpensivelyCalculatedValue<TValue> {
        private Task<TValue> valueCalculation;
        private ExpensivelyCalculatedValue<TValue> objectBehind;

        public ExpensivelyCalculatedValueProxy(ExpensivelyCalculatedValue<TValue> objectBehind) {
            this.objectBehind = objectBehind;
        }

        public void triggerBackgroundCalculation() {
            valueCalculation = Task<TValue>.Factory.StartNew(() => objectBehind.calculate());
        }
        
        public TValue calculate() {
            if (valueCalculation == null) // calculation in the background not triggered yet
                triggerBackgroundCalculation();
            return valueCalculation.Result;
        }
    }
}