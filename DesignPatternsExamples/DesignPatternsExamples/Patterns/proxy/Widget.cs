namespace Patterns.proxy {
    public class Widget : WidgetIfc {
        public void doSomething() {
            // does something that could maybe run over a slow connection
        }
    }
}