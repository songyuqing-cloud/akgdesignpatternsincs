
using System.Threading;

namespace Patterns.proxy {
    public class StatisticalValue : ExpensivelyCalculatedValue<double> {
        private string heavyStuff;

        public StatisticalValue(string heavyStuff) {
            this.heavyStuff = heavyStuff;
        }
        
        public double calculate() {
            // take the heavyStuff and perform this heavy calculation...
            Thread.Sleep(2000);
            return 42.0;
        }

    }

    public static class StatisticalCalculations {
        
        public static double calculateStatistics(string heavyStuff) {
            // take the heavyStuff and perform this heavy calculation...
            Thread.Sleep(2000);
            return 42.0;
        }
    }
    
}