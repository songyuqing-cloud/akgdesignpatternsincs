using System.Threading;

namespace Patterns.proxy {
    public class WidgetSlowConnectionSimulator : WidgetIfc {
        private WidgetIfc widgetBehind;
        public int millisDelay;

        public WidgetSlowConnectionSimulator(WidgetIfc widgetBehind) {
            this.widgetBehind = widgetBehind;
        }
        public void doSomething() {
            Thread.Sleep(millisDelay);
            widgetBehind.doSomething();
        }
    }
}