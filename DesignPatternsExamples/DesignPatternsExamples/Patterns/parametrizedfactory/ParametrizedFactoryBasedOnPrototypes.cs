using System.Collections.Generic;
using Patterns.prototype;

namespace Patterns.parametrizedfactory {
    public class ParametrizedFactoryBasedOnPrototypes {
        private Dictionary<Create, Widget> prototypes = new Dictionary<Create, Widget>();

        public Widget create(Create createThat) {
            Widget prototype;
            return !prototypes.TryGetValue(createThat, out prototype) ? null : prototype.createDuplicate();
        }

        public void registerPrototype(Create creatorFor, Widget prototype) {
            prototypes.Add(creatorFor, prototype);
        }
    }
}