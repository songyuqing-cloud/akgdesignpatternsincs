namespace Patterns.parametrizedfactory {
    public enum Create {
        WidgetA,
        WidgetB
    }
}