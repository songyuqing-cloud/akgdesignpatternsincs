

using System;

namespace Patterns.mediator {
    public class FontChooserMediator {
        private bool dealingWithFontChangedEvent;

        public FontSelector fontSelector { get; private set; }
        public TextExamplePresenter textExamplePresenter { get; private set; }

        public void buildDialog() {
            fontSelector = new FontSelector();
            textExamplePresenter = new TextExamplePresenter();

            fontSelector.fontChangedEvent += onFontSelectorChangedFontNotification;
            textExamplePresenter.exampleFontChangedEvent += onTextExamplePresenterChangedFontNotification;
            textExamplePresenter.exampleTextChangedEvent += onTextExamplePresenterChangedTextNotification;
        }

        private void recursionProtectedBy(ref bool flag, Action doThat) {
            if (flag) // avoid endless recursion
                return;
            try {
                flag = true;
                doThat();
            }
            finally {
                flag = false;
            }
        }

        private void onFontSelectorChangedFontNotification(Font previousFont, Font currentFont) {
            recursionProtectedBy(ref dealingWithFontChangedEvent, () => {
                textExamplePresenter.exampleFont = currentFont; // update the example presenter
                // perform all additional stuff here...
            });
        }

        private void onTextExamplePresenterChangedFontNotification(Font previousFont, Font currentFont) {
            recursionProtectedBy(ref dealingWithFontChangedEvent, () => {
                fontSelector.selectedFont = currentFont; // update the font selector
                // perform all additional stuff here...
            });
        }

        private void onTextExamplePresenterChangedTextNotification(string previousText, string currentText) {
            // perform all mediation stuff here...
        }

    }
}