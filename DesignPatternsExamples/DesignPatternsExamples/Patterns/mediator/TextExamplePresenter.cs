using System;

namespace Patterns.mediator {
    public class TextExamplePresenter {
        private string exampleTextField = "example";
        public string exampleText {
            get { return exampleTextField; }
            set {
                var previous = exampleTextField;
                exampleTextField = value;
                exampleTextChangedEvent?.Invoke(previous, exampleTextField);
            }
        }

        private Font exampleFontField = Font.Helvetica;
        public Font exampleFont {
            get { return exampleFontField; }
            set {
                var previous = exampleFontField;
                exampleFontField = value;
                exampleFontChangedEvent?.Invoke(previous, exampleFontField);
            }
        }
        
        public Action<Font, Font> exampleFontChangedEvent;
        public Action<string, string> exampleTextChangedEvent;
    }
}