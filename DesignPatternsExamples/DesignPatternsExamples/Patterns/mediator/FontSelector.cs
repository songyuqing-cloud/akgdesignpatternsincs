using System;

namespace Patterns.mediator {
    public class FontSelector {
        private Font selectedFontField = Font.Helvetica;
        public Font selectedFont {
            get { return selectedFontField; }
            set {
                var previous = selectedFontField;
                selectedFontField = value;
                fontChangedEvent?.Invoke(previous, selectedFont);
            }
        }
        public Action<Font, Font> fontChangedEvent;
    }
}