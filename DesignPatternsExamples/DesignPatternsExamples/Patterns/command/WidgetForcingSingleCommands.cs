using System;

namespace Patterns.command {
    public class WidgetForcingSingleCommands {
        private Action<string> cmdToShowNotification;

        public void setCmdToShowNotification(Action<string> doThat) {
            cmdToShowNotification = doThat;
        }

        public void doSomething() {
            // ... do whatever has to be done here...

            // and now show the notification that you did it
            cmdToShowNotification?.Invoke("performed doSomething");
        }
    }
}