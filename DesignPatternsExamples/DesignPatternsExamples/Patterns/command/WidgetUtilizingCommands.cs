using System;

namespace Patterns.command {
    public class WidgetUtilizingCommands {
        public Action<string> cmdToShowNotification;

        public void doSomething() {
            // ... do whatever has to be done here...

            // and now show the notification that you did it
            cmdToShowNotification?.Invoke("performed doSomething");
        }
    }
}