namespace Patterns.decorator {
    public interface WidgetIfc {
        int justAValue { get; set; }
    }
}