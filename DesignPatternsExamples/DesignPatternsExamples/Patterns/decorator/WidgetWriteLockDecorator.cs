using System;

namespace Patterns.decorator {
    public class WidgetWriteLockDecorator : WidgetIfc {
        private WidgetIfc decoratedObj;

        public WidgetWriteLockDecorator(WidgetIfc decoratedObj) {
            this.decoratedObj = decoratedObj;
        }
        public int justAValue {
            get { return decoratedObj.justAValue; }
            set { throw new InvalidOperationException("widget is write-protected"); }
        }
    }
}