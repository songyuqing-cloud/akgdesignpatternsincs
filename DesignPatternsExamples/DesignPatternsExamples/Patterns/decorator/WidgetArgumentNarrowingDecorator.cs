using System;

namespace Patterns.decorator {
    public class WidgetArgumentNarrowingDecorator : WidgetIfc {
        private WidgetIfc decoratedObj;
        private int minValue;
        private int maxValue;

        public WidgetArgumentNarrowingDecorator(WidgetIfc decoratedObj, int minValue, int maxValue) {
            this.decoratedObj = decoratedObj;
            this.minValue = minValue;
            this.maxValue = maxValue;
        }
        public int justAValue {
            get { return decoratedObj.justAValue; }
            set {
                if (value < minValue)
                    throw new ArgumentException("value below allowed minimum");
                if (value > maxValue)
                    throw new ArgumentException("value above allowed maximum");
                decoratedObj.justAValue = value;
            }
        }
    }
}