using System;

namespace Patterns.decorator {
    public class WidgetLoggingDecorator : WidgetIfc {
        private Action<string> writeLogEntry;
        private WidgetIfc decoratedObj;

        public WidgetLoggingDecorator(WidgetIfc decoratedObj, Action<string> writeLogEntry) {
            this.decoratedObj = decoratedObj;
            this.writeLogEntry = writeLogEntry;
        }
        public int justAValue {
            get {
                writeLogEntry("getter of justAValue called");
                return decoratedObj.justAValue;
            }
            set {
                writeLogEntry("setter of justAValue called");
                decoratedObj.justAValue = value;
            }
        }
    }
}