namespace Patterns.walker {
    public class SkipWalker<TValue> : Walker<TValue> {
        private Walker<TValue> src;
        private int remainingElementsToSkip;

        public SkipWalker(Walker<TValue> src, int numElementsToSkip) {
            this.src = src;
            remainingElementsToSkip = numElementsToSkip;
        }

        public bool hasCurrent {
            get {
                if (remainingElementsToSkip > 0)
                    skipInitialElements();
                return src.hasCurrent;
            }
        }

        public TValue currentValue {
            get {
                if (remainingElementsToSkip > 0)
                    skipInitialElements();
                return src.currentValue;
            }
        }

        public bool walkToNext() {
            if (remainingElementsToSkip > 0)
                skipInitialElements();
            return src.walkToNext();
        }

        private void skipInitialElements() {
            while (remainingElementsToSkip > 0 && src.hasCurrent) {
                src.walkToNext();
                --remainingElementsToSkip;
            }
        }
    }
}