namespace Patterns.walker {
    public interface BidirectionalWalker<TValue> : Walker<TValue> {
        bool walkToPrevious();
        bool walkToFirst();
        bool walkToLast();
    }
}