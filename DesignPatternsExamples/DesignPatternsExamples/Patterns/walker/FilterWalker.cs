using System;

namespace Patterns.walker {
    public class FilterWalker<TValue> : Walker<TValue> {
        private bool isInitialized = false;
        private Walker<TValue> src;
        private Func<TValue, bool> takeThisObj;
        public FilterWalker(Walker<TValue> src, Func<TValue, bool> takeThisObj) {
            this.src = src;
            this.takeThisObj = takeThisObj;
        }

        public bool hasCurrent {
            get {
                if (!isInitialized) {
                    skipNonMatchingElements();
                    isInitialized = true;
                }
                return src.hasCurrent;
            }
        }

        public TValue currentValue {
            get {
                if (!isInitialized) {
                    var dummy = hasCurrent; // initializes everything...
                }
                return src.currentValue;
            }
        }
        public bool walkToNext() {
            src.walkToNext();
            skipNonMatchingElements();
            return src.hasCurrent;
        }

        private void skipNonMatchingElements() {
            while (src.hasCurrent) {
                if (takeThisObj(src.currentValue))
                    break;
                src.walkToNext();
            }
        }
    }
}