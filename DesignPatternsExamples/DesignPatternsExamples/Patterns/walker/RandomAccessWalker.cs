namespace Patterns.walker {
    public interface RandomAccessWalker<TKey, TValue> : BidirectionalWalker<TValue> {
        bool walkTo(TKey key);
        TKey currentKey { get;  }
    }
}