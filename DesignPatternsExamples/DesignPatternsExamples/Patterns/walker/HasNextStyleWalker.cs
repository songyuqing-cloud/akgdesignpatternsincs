using System;

namespace Patterns.walker {

    internal class ReferenceWrapper<TValue> {
        public TValue value;
        public ReferenceWrapper(TValue value) { this.value = value; }
    }
        
    
    public class HasNextStyleWalker<TValue> {
        private Walker<TValue> src;
        private ReferenceWrapper<TValue> nextObj;

        public HasNextStyleWalker(Walker<TValue> src) {
            this.src = src;
            if (src.hasCurrent)
                nextObj = new ReferenceWrapper<TValue>(src.currentValue);
        }
        public bool hasNext() {
            if (nextObj != null) return true;
            if (src.walkToNext())
                nextObj = new ReferenceWrapper<TValue>(src.currentValue);
            return nextObj != null;
        }

        public TValue next {
            get {
                if (!hasNext())
                    throw new InvalidOperationException("walker has no next element");
                var result = nextObj.value;
                nextObj = null;
                return result;           
            }
        }
    }
}