using System;
using System.Collections.Generic;

namespace Patterns.walker {
    public class ListWalker<TValue> : RandomAccessWalker<int, TValue> {
        private List<TValue> src;
        private int currentIdx;

        public ListWalker(List<TValue> src) { this.src = src; }
        
        public bool hasCurrent { get { return currentIdx >= 0 && currentIdx < src.Count; } }
        public TValue currentValue { get { return src[currentIdx]; } }
        public bool walkToNext() {
            ++currentIdx;
            return hasCurrent;
        }

        public bool walkToPrevious() {
            --currentIdx;
            return hasCurrent;
        }

        public bool walkToFirst() {
            currentIdx = 0;
            return hasCurrent;
        }

        public bool walkToLast() {
            currentIdx = src.Count - 1;
            return hasCurrent;
        }

        public bool walkTo(int key) {
            currentIdx = key;
            return hasCurrent;
        }

        public int currentKey {
            get {
                if (!hasCurrent)
                    throw new InvalidOperationException("walker has no current element");
                return currentIdx;
            }
        }
    }
}