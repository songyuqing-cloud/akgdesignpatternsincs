namespace Patterns.walker {
    public interface Walker<TValue> {
        bool hasCurrent { get; }
        TValue currentValue { get; }
        bool walkToNext();
    }
}